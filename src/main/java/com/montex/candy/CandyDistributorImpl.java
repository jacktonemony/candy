package com.montex.candy;

import java.util.Arrays;

/**
 * Class for distributing candies ensuring that for two adjacent kids, kid with
 * higher rating gets more candy while keeping maximum candies handed at a
 * minimum
 * 
 * @author Jacktone
 * 
 */
public class CandyDistributorImpl implements ICandyDistributor {

	private final int[] ratings;
	private int totalCandies = 0;
	private final int[] candies;

	enum DistMode {
		DESC, ASC, PLATEAU
	}

	public CandyDistributorImpl(int[] ratings) {
		//Assert.notNull(rating);
        if(null==ratings)           
            throw new IllegalArgumentException("rating cannot be null");
		this.ratings = ratings;
		this.candies = new int[this.ratings.length];
		Arrays.fill(candies, 1);// ensure everyone gets a sweet (even if the
								// programmer messes :-))
		distributeCandy(getDistMode(0), 0);
		sumCandies();
	}

	private void sumCandies() {
		for (int i : candies)
			totalCandies += i;
	}

	private DistMode getDistMode(int i) {
		if ((i + 1 > ratings.length - 1))
			return null;
		if (ratings[i] > ratings[i + 1])
			return DistMode.DESC;
		else if (ratings[i] < ratings[i + 1])
			return DistMode.ASC;
		return DistMode.PLATEAU;
	}

	private void distributeCandy(DistMode dm, int start) {
		int end = start;
		boolean modeChange = false;
		while (modeChange == false && end < ratings.length) {
			end++;
			if (end < ratings.length - 1) {
				if (getDistMode(end) != dm)
					modeChange = true;
			}
		}
		int max = (end - start) + 1;
		if (null != dm) {
			switch (dm) {
			case DESC:
				int temp = start;
				if (start <= ratings.length - 1) {
					candies[start] = max;
				}
				while (start <= end) {
					if (start + 1 <= ratings.length - 1)
						candies[start + 1] = candies[start] - 1;
					start++;
				}
				if (temp > 0 && candies[temp - 1] >= candies[temp]
						&& ratings[temp] > ratings[temp - 1])
					candies[temp] = candies[temp - 1] + 1;
			case ASC:
				if (start <= ratings.length - 1)
					candies[start] = 1;
				while (start + 1 <= ratings.length - 1 && start <= end) {
					candies[start + 1] = candies[start] + 1;
					start++;
				}
			case PLATEAU:
				while (start + 1 <= ratings.length - 1 && start <= end) {
					candies[start + 1] = 1;
					start++;
				}
			}
		}
		if (end < ratings.length - 1 && null != dm)
			distributeCandy(getDistMode(end), end);
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.montex.candy.ICandyDistributor#getTotalCandies()
	 */
	public int getTotalCandies() {
		return totalCandies;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.montex.candy.ICandyDistributor#getRatings()
	 */
	public int[] getRatings() {
		return ratings;
	}

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.montex.candy.ICandyDistributor#getCandies()
	 */
	public int[] getCandies() {
		return candies;
	}
	

	/*
	 * (non-Javadoc)
	 * @see com.montex.candy.ICandyDistributor#displayDistribution()
	 */
	public void displayDistribution() {
        System.out.println("Child\t\tRating\t\tcandys");
        int i = 0;
        StringBuilder sb = new StringBuilder();
        while (i < ratings.length) {
            sb.append((i + 1)).append("\t\t").append(ratings[i]).append("\t\t").append(candies[i]).append("\n");
            i++;
        }
        sb.append("Total number of candies is ").append(totalCandies);
        System.out.println(sb.toString());
    }

}
