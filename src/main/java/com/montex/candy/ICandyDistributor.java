package com.montex.candy;

/**
 * 
 * @author Jacktone
 * 
 */
public interface ICandyDistributor {
	/**
	 * Returns the number of candies distributed to the children
	 * 
	 * @return the number of candies distributed
	 */
	public int getTotalCandies();

	/**
	 * Returns the array of ratings per child, child position is represented by
	 * the index ,rating by value
	 * 
	 * @return the ratings array
	 */
	public int[] getRatings();

	/**
	 * Returns the candies as distributed per child, child position is
	 * represented by the index candies by value
	 * 
	 * @return the candies array after distribution
	 */
	public int[] getCandies();
    /**
     * Outputs distribution to user
     */
	void displayDistribution();

}
