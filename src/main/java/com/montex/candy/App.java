package com.montex.candy;

import java.util.Scanner;

/**
 * Entry point to candy distributor
 * 
 * @author Jacktone
 *
 */
public class App {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		try {
			System.out.println("Please enter numbers starting with number of kids\n"
					+ "followed by rating e.g 12, 45, 16 -1 to end input");
			int n = readInput();
			while (n != -1 && n <= 0) {
				System.out.println("Please input number greater than 0, -1 to exit");
				n = readInput();
			}
			if (n == -1) {
				System.out.println("No data entered exiting....");
				System.exit(0);
			}
			int[] ratings = new int[n];
			int i = 0;
			while (i < n) {
				ratings[i] = readInput();
				i++;
			}
			ICandyDistributor candyDistributor = new CandyDistributorImpl(ratings);
			candyDistributor.displayDistribution();
		} finally {
			scanner.close();
		}
	}

	// Assuming correct input from user for the moment.
	// TO DO handle bad input
	private static int readInput() {
		scanner = new Scanner(System.in);
		boolean b = true;
		int x = 0;
		while (b) {
			try {
				x = scanner.nextInt();
				b = false;
			} catch (NumberFormatException ex) {
				System.out.println("Please input valid number");
			}
		}
		return x;

	}
}
