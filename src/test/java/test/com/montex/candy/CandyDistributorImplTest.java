package test.com.montex.candy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.montex.candy.ICandyDistributor;
import com.montex.candy.CandyDistributorImpl;

/**
 *
 * @author Jacktone
 *
 */
public class CandyDistributorImplTest {
    List<int[]> ratings = new ArrayList<int[]>();

    @Before
    public void setUp() throws Exception {

        ratings.add(new int[] { 5, 3, 4, 5, 7, 8, 9, 2, 2 });
        ratings.add(new int[] { 1, 3, 4, 5, 7, 8, 9, 2, 2 });
        ratings.add(new int[] { 5, 5, 5, 5, 5, 5, 5, 1, 1 });
        ratings.add(new int[] { 5, 5, 5, 5, 5, 5, 5, 1, 2 });
        ratings.add(new int[] { 5, 3, 4, 5, 7, 8, 9, 2, 2 });
        ratings.add(new int[] { 10, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10 });
        ratings.add(new int[] { 5, 3, 4, 5, 1, 1, 3, 4, 5 });
        ratings.add(new int[] { 9, 3, 9, 1, 9, 9, 9, 1, 1 });
        ratings.add(new int[] { 2, 2 });
        ratings.add(new int[] { 7 });
        ratings.add(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
        ratings.add(new int[] { 1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1 });
        ratings.add(new int[] { 1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1, 2 });
        ratings.add(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
        ratings.add(new int[] { 12, 11, 10, 9, 8, 7, 6, 5, 4, 13, 2, 1 });
        ratings.add(new int[] { 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 });
        ratings.add(new int[] { 9, 9, 9, 9, 9, 9, 9, 9, 9 });
        ratings.add(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 });
        ratings.add(new int[] { 5, 3, 3, 5, 7, 8, 9, 2, 2 });
        ratings.add(new int[] { 5, -3, 3, -5, 7, 8, -9, 0, 2 });
        ratings.add(new int[] { 0, -3, 3, -5, 7, 8, -9, 0, 2 });
        ratings.add(new int[] { 0, 0, 3, -5, 7, 8, -9, 0, 2 });
        ratings.add(new int[] { 2, 0, 3, -5, 7, 0, -9, 0, 2 });
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCandyDistribution() {
        // test arrays added in setup
        for (int[] r : ratings) {
            doForwardAndBackwardCheck(r);
        }

        // test some random array
        Random rand = new Random();
        int size = rand.nextInt(50);
        int[] ratingRandom = new int[size];
        int i = 0;
        while (i < size) {
            ratingRandom[i] = rand.nextInt(100);
            i++;
        }
        doForwardAndBackwardCheck(ratingRandom);
        // test minimum for equal rating of zero
        ICandyDistributor cd = new CandyDistributorImpl(new int[4]);
        for (int j : cd.getCandies()) {
            assertEquals(
                    "Single candy for every child with equal rating of zero",
                    1, j);
        }
        assertEquals("Only four candy distributed", 4, cd.getTotalCandies());

    }

    private void doForwardAndBackwardCheck(int[] rating1) {
        ICandyDistributor cd = new CandyDistributorImpl(rating1);
        int[] candies = cd.getCandies();
//        int[] ratings = cd.getRatings();
//        System.out.println("Child\t\tRating\t\tCandies");
//        for (int i = 0; i < candies.length; i++) {
//            System.out.println(i + "\t\t" + ratings[i] + "\t\t" + candies[i]);
//        }

        for (int i=0;i<rating1.length;i++) {
            if (i + 1 < rating1.length && rating1[i] > rating1[i + 1]) {
                assertTrue(
                        "Every child with higher rating than the next gets more candy >> ",
                        candies[i] > candies[i + 1]);
            }
        }

        for (int i = rating1.length - 1; i > 0; i--) {
            if (rating1[i] > rating1[i - 1]) {
                assertTrue(
                        "Every child with higher rating than the previous gets more candy"
                                + i, candies[i] > candies[i - 1]);
            }

        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullRating() {
        int[] ratings2 = null;
        ICandyDistributor cd = new CandyDistributorImpl(ratings2);
    }

}